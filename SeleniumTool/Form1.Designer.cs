﻿namespace SeleniumTool
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnImportSel = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.btnImport = new System.Windows.Forms.Button();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.rtxtHTMLFile = new System.Windows.Forms.RichTextBox();
            this.btnAdd = new System.Windows.Forms.RichTextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnImportSel
            // 
            this.btnImportSel.Location = new System.Drawing.Point(12, 12);
            this.btnImportSel.Name = "btnImportSel";
            this.btnImportSel.Size = new System.Drawing.Size(75, 23);
            this.btnImportSel.TabIndex = 0;
            this.btnImportSel.Text = "Starter File";
            this.btnImportSel.UseVisualStyleBackColor = true;
            this.btnImportSel.Click += new System.EventHandler(this.btnImportSel_Click);
            // 
            // textBox1
            // 
            this.textBox1.Enabled = false;
            this.textBox1.Location = new System.Drawing.Point(93, 14);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(365, 20);
            this.textBox1.TabIndex = 1;
            // 
            // btnImport
            // 
            this.btnImport.Location = new System.Drawing.Point(464, 11);
            this.btnImport.Name = "btnImport";
            this.btnImport.Size = new System.Drawing.Size(75, 23);
            this.btnImport.TabIndex = 2;
            this.btnImport.Text = "CSV File";
            this.btnImport.UseVisualStyleBackColor = true;
            this.btnImport.Click += new System.EventHandler(this.btnImport_Click);
            // 
            // textBox2
            // 
            this.textBox2.Enabled = false;
            this.textBox2.Location = new System.Drawing.Point(545, 11);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(365, 20);
            this.textBox2.TabIndex = 3;
            // 
            // rtxtHTMLFile
            // 
            this.rtxtHTMLFile.Location = new System.Drawing.Point(12, 40);
            this.rtxtHTMLFile.Name = "rtxtHTMLFile";
            this.rtxtHTMLFile.Size = new System.Drawing.Size(446, 424);
            this.rtxtHTMLFile.TabIndex = 4;
            this.rtxtHTMLFile.Text = "";
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(464, 40);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(446, 424);
            this.btnAdd.TabIndex = 5;
            this.btnAdd.Text = "";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 470);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 6;
            this.button1.Text = "Add Users";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(919, 510);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.rtxtHTMLFile);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.btnImport);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.btnImportSel);
            this.Name = "frmMain";
            this.Text = "Selenium Tool";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnImportSel;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button btnImport;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.RichTextBox rtxtHTMLFile;
        private System.Windows.Forms.RichTextBox btnAdd;
        private System.Windows.Forms.Button button1;
    }
}

