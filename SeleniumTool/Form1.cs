﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace SeleniumTool
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
        }

        string SELFile;
        string CSVFile;

        private void btnImportSel_Click(object sender, EventArgs e)
        {
            OpenFileDialog OFD = new OpenFileDialog();

            OFD.InitialDirectory = "/";
            OFD.RestoreDirectory = true;
            if (OFD.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    SELFile = OFD.FileName;
                    textBox1.Text = SELFile;
                    rtxtHTMLFile.LoadFile(OFD.FileName, RichTextBoxStreamType.PlainText);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }
        }

        private void btnImport_Click(object sender, EventArgs e)
        {
            OpenFileDialog OFD = new OpenFileDialog();

            OFD.InitialDirectory = "/";
            OFD.RestoreDirectory = true;
            if (OFD.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    CSVFile = OFD.FileName;
                    textBox2.Text = SELFile;
                    btnAdd.LoadFile(OFD.FileName, RichTextBoxStreamType.PlainText);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
            List<String> SelenuimFile = File.ReadAllLines(SELFile).Reverse().Skip(3).ToList();
            string[] CSVFileData = File.ReadAllLines(CSVFile);
            foreach(string x in CSVFileData)
            {
                string[] y = x.Split(',');
                SelenuimFile.Add("<tr> <td>open</td> <td>/tooltime/comp10066/l3/login.php</td> <td></td> </tr>");
                SelenuimFile.Add("<tr> <td>type</td> <td>id=username</td> <td>" + y[0] + "</td> </tr>");
                SelenuimFile.Add("<tr> <td>type</td> <td>name=password</td> <td>" + y[1] + "</td> </tr>");
                SelenuimFile.Add("<tr> <td>clickAndWait</td> <td>name=Submit</td> <td></td> </tr>");
                SelenuimFile.Add("<tr> <td>assertText</td> <td>id=loginname</td> <td>exact:User: "+ y[0] +"</td> </tr>");
                SelenuimFile.Add("<tr> <td>clickAndWait</td> <td>link=Logout</td> <td></td> </tr>");
            }
            SelenuimFile.Add("</tbody></table></body></html>");
            File.WriteAllLines("output.html", SelenuimFile);
        }
    }
}
